Source: alass
Section: video
Priority: optional
Maintainer: Jonas Smedegaard <dr@jones.dk>
Build-Depends:
 debhelper-compat (= 13),
 dh-cargo,
 ffmpeg <!nocheck>,
 help2man <!nodoc>,
 librust-byteorder-1+default-dev,
 librust-cast-dev,
 librust-c2-chacha-dev,
 librust-cc-dev,
 librust-cfg-if-0.1-dev,
 librust-clap-2+default-dev,
 librust-ctrlc-3+default-dev <!nocheck>,
 librust-either-dev,
 librust-encoding-rs-0.8+default-dev,
 librust-enum-primitive-dev,
 librust-failure-0.1+default-dev,
 librust-lazy-static-dev,
 librust-libc-0.2+default-dev,
 librust-log-dev,
 librust-num-iter-dev,
 librust-pbr-1+default-dev,
 librust-rmp-serde-1+default-dev,
 librust-serde-1+default-dev,
 librust-serde-1+derive-dev,
 librust-serde-1+rc-dev,
 librust-serde-json-1+default-dev,
 librust-termion-dev,
 librust-threadpool-1+default-dev <!nocheck>,
 librust-ucd-util-dev,
 librust-utf8-ranges-dev,
Standards-Version: 4.6.1
Homepage: https://github.com/kaegi/alass
Vcs-Git: https://salsa.debian.org/debian/alass.git
Vcs-Browser: https://salsa.debian.org/debian/alass
Rules-Requires-Root: no

Package: alass
Architecture: any
Depends:
 ffmpeg,
 ${misc:Depends},
 ${shlibs:Depends},
Built-Using:
 ${cargo:Built-Using},
XB-X-Cargo-Built-Using: ${cargo:X-Cargo-Built-Using}
Description: automatic language-agnostic subtitle synchronization
 alass is a command line tool
 to synchronize subtitles to movies.
 .
 It can automatically correct
  * constant offsets
  * splits due to advertisement breaks, directors cut, ...
  * different framerates
 .
 The alignment process is not only fast and accurate,
 but also language-agnostic.
 This means you can align subtitles to movies in different languages.
